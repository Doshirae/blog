---
authors: ["zilkos"]
title: "Le chef d''orchestre"
date: 2016-05-19
tags: ["Musique", "Orchestre"]
toc: true
---

# Qui est cette personne qui agite les bras tel le poulpe en fuite ?

Le Chef d'Orchestre aussi appelé conducteur d'orchestre, personne incontournable ou presque dans les représentations de musique dite classique. Beaucoup pensent qu'il ne sert à rien, mis à part gagner plein d'argent en bougeant les bras, qu'il soit pourvu d'une baguette ou non.

Et bien non, cher visiteur, son rôle est vital au bon déroulement des choses et nécessaire pour avoir une bonne interprétation de l'œuvre jouée.

Voyons tout ça !

## Les orchestres

Être chef de quelque chose c'est bien, encore faut-il savoir de quoi. Nous entendons ça et là _orchestre harmonique_, _orchestre philharmonique_, _orchestre symphonique_. Tous ces noms, souvent utilisés au hasard, représentent bien quelque chose, même _philharmonique_ qui est si joli à dire pour briller en société.

### Orchestre symphonique

Bonne nouvelle, l'orchestre symphonique désigne la même chose que l'orchestre philharmonique. Ce type d'orchestre est composé de quatre familles d'instruments, à savoir :

 * Les cordes
   - Premiers violons
   - Deuxièmes violons
   - Les altos
   - Les violoncelles
   - Les contrebasses
 * Les bois
   - Les flûtes
   - Les hautbois
   - Les clarinettes
   - Les saxophones (pas toujours !)
   - Les bassons
 * Les cuivres
   - Les trompettes
   - Les cors
   - Les trombones
   - Les trombones basses
   - Les tubas
 * Les percussions
   - Xylophone, vibraphone, glockenspiel, timables, grosse caisse, etc. Tout dépend des besoins
 
Alors, tout dépend des besoins de l'œuvre, mais globalement on peut retrouver la liste indiquée dessus et d'autres instruments. En sachant qu'il y a du monde, en moyenne on trouve une soixantaine de musiciens pour les cordes, une grosse vingtaine de bois, une quinzaine de cuivre et pour les percussions, tout dépend de l'œuvre. On peut facilement monter à plus d'une centaine de musiciens.

Donc ça fait quand même beaucoup de personnes devant le chef d'orchestre qui je le rappelle est dos à l'auditoire. Le but du jeu, c'est de mettre tout le monde d'accord et que chacun fasse son boulot (et si possible, qu'il le fasse bien).

### Orchestre d'harmonie

L'orchestre d'harmonie quant à lui est très similaire à l'orchestre symphonique, **sauf** qu'il ne comporte que trois familles: les bois, les cuivres et les percussions. Point de cordes frottées dans cet ensemble, même si des petits malins s'amusent de temps en temps à mettre des contrebasses, une harpe, un piano (famille des cordes frappées) voire même un célesta dans la formation. Attention, le célesta est un idiophone, donc de la famille des percussions.

> Un idiophone est forcément de la famille des percussions, en effet chez les idiophones le son est produit par le matériau de l'instrument lors d'une frappe.

### Orchestre de chambre

Celui-ci est déjà plus petit, seulement une trentaine de musiciens, tout simplement car ce type d'orchestres donnait des représentations dans des lieux généralement intimistes, comme des salons, ou de petites salles de concert, mais aussi, des églises (qui pour le coup n'ont rien d'intime).

Contrairement à ce que l'on pourrait penser, non, l'orchestre de chambre ne joue pas de la musique de chambre. On y joue des cantates, des oratorios, des suites.

## Un peu de vocabulaire

Tant que j'y suis, j'en profite pour vous présenter quelques mots qu'on retrouve souvent quand on aborde les orchestres et la composition d'un orchestre (attention, je parle uniquement de musique dite classique, dans le jazz on en trouve généralement trois: le big band, le jazz band et l'orchestre de jazz).

### Pupitre

Ce mot a plusieurs sens rien que dans le domaine de la musique. En premier lieu, ça désigne un dispositif qui permet de soutenir la partition. Un trépied et une partie plate pour poser une partition, simple. Il peut aussi désigner l'emplacement du chef d'orchestre et on l'utilise souvent dans l'expression "C'est l'orchestre _machin_ avec _bidule_ au pupitre". _Bidule_ étant le chef d'orchestre en question, positionné sur un genre de petite estrade et de ce point il est visible par **tous** les instrumentistes, qu'ils soient 25 ou 150. C'est vital qu'il soit visible de tous.

Et enfin, un pupitre peut désigner un ensemble de musicien qui joue la même partition (la même partie). Par exemple dans un orchestre symphonique à 16 premiers violons, plutôt que de d'égrainer les noms des musiciens ou de dire _"Les mecs devant à gauche avec les violons"_, on dit _"le pupitre des premiers violons"_. À ne pas confondre avec _"le premier violon"_ tout court qui désigne une personnalité plus douée que les autres qui joue avec le pupitre des premiers violons mais qui s'occupe des _soli_ (un solo, des _soli_). 

Regardez un concert, vous verrez que **tous** les violonistes font aller et venir leur archer dans le **même** sens. Ça, par exemple, c'est le boulot du premier violon de dire dans quel sens on commence, soit on tire, soit on pousse, l'intensité des vibratos et autres éléments communs aux violonistes. Et oui, dans un orchestre, il ne suffit pas "uniquement" de jouer la partition, il y a toute une hiérarchie de responsabilités, des codes communs, bref c'est similaire à une petite société, les manifestants en moins.


### Soliste

C'est la personne (comme vu plus haut, souvent le "premier" d'un pupitre) qui doit assurer une partie musicale seul. Considérez ça comme un solo, si vous préférez (mais que sur certaines parties, le reste du temps et selon les œuvres jouées, il a le même rôle musical qu'un membre de pupitre). 

### Ensemble

Un ensemble musical, c'est tout simplement un groupement de musiciens qui sont habitués à jouer ensemble, peut importe leur nombre tant qu'il est supérieur à un (autrement, c'est un soliste).

Sa détermination est particulière, tant qu'ils sont entre 2 et 10. Au delà, c'est un orchestre, tout simplement.

  - Deux: un duo (duet en jazz)
  - Trois: un trio
  - Quatre: un quatuor (un quartet en jazz)
  - Cinq: un quintette (quintet en jazz)
  - Six: un sextuor (sextet en jazz)
  - Sept: un septuor (septet en jazz)
  - Huit: un octuor (octet en jazz, coucou aux amis barbus)
  - Neuf (si si ça existe): un nonet (nonette en jazz)
  - Dix (si, si, vraiment): un dixtuor (pas d'équivalent en jazz, ça fait trop de monde déjà !)
	
### Tessiture

La tessiture c'est tout simplement l'ensemble des notes que peut jouer un instrument à un volume et à une qualité de son identique.

Par exemple pour les voix (humaines) on trouve, du plus grave au plus aiguë (du plus "bas" au plus "haut") et en mélangeant homme et femme:

  - Basse
  - Baryton
  - Ténor
  - Contre-ténor / tenor
  - Contralto / alto
  - Mezzo-soprano
  - Soprano
  
Par exemple le ténor va généralement aller de do2 jusqu'à do4, bien évidemment, ça reste augmentable en travaillant. Différents instruments d'une même famille disposent de tessitures différentes. Un petit exemple:

Si on prend un saxophone, qui je le rappelle fait partie de la famille des bois et non pas des cuivres, nous avons les tessitures qui varient énormément. Le saxophone le plus grave étant le contrebasson, une grosse bête quand même, il fait 1,20m non déplié et si on déplie le tout il monte jusqu'à 5,50m... C'est l'instrument le plus grave des bois, tout simplement.

Il dispose d'environ trois octaves en partant de la fin de do-2 pour aller entre do2 et do3.

Le piccolo est à l'extrême opposé, instrument le plus aiguë des bois, allant de do4 jusqu'à do7. Entre ces instruments, on retrouve:

  - Saxophone basse
  - Basson
  - Saxophone baryton
  - Saxophone ténor
  - Saxophone alto
  - Clarinette en si bémol
  - Cor anglais
  - Saxophone soprano
  - Hautbois
  - Flûte
  - Et d'autres...
  
Maintenant que tout ceci est posé, non pas que ce soit important par la suite mais c'est toujours sympa de le savoir, passons au rôle du poulpe qui est quand même le sujet de l'article.

## Les rôles du chef d'orchestre

### Mesure et justesse

Le rôle prioritaire et vital du chef d'orchestre, histoire que le parterre de musiciens soit un minimum convainquant, c'est de faire en sorte que tout le monde joue **juste** et en **mesure**. 

Vous allez me dire quelque chose comme: _" Mouais, boh si les instruments sont accordés, il se touche le mec en fait, non ?_ Que nenni ! Il faut que le chef d'orchestre soit doué d'une grande oreille pour détecter les éventuelles variations rythmiques d'un ou plusieurs musiciens lors d'une accélération ou d'un ralentissement du temps sur une partie spécifique, mais aussi détecter une certaine propreté et justesse sur certaines phrases. Il veille aussi au niveau sonore de chaque instrument, hors de question qu'un violoncelle prenne le dessus sur les autres. Et autant vous dire qu'il peut y avoir couramment une centaine de musiciens devant lui, avec divers instruments disposant de différentes tessitures. 

Sans ça, ce n'est plus de la musique, c'est de la bouillie, sans aucune saveur ni nuance.

### Un lecteur hors pair

Véritable big brother des partitions, il dispose de toutes les partitions des musiciens. Il s'agit là d'un réel travail de lecture, même si bien sûr un chef d'orchestre connaît par cœur l'œuvre à interpréter. Non seulement d'un point de vue du solfège, mais aussi en rapport avec l'œuvre elle-même, ce qu'elle dit, dans quel contexte elle a été écrite et pourquoi elle existe. Ça présuppose une immense culture musicale et historique. C'est à lui de déterminer, après étude de l'œuvre, la direction de l'**interprétation** à prendre et seul lui à le dernier mot à ce sujet.

Pour vous donner un ordre d'idée, une "partition" de chef d'orchestre ressemble à ça:

{{% center %}}
![Partition d'un chef d'orchestre](/static/img/musique/partition.jpg)
{{% /center %}}

Donc le chef d'orchestre dispose des notes et des indications de jeu de tout le monde. Clairement, il ne faut pas passer trois secondes à se tâter pour savoir si c'est un fa ou un sol qu'il faut entendre sur tel ou tel instrument.


### Le gardien fédérateur des émotions

Un chef d'orchestre a en face de lui une somme d'humains. Tous ces humains disposent de sentiments, d'une manière de voir les choses, de les ressentir, de les entendre et de les transposer en musique.

Le chef d'orchestre est là pour fédérer cette somme de sentiments, pour donner une orientation **commune** à l'interprétation, généralement fort de son expérience et de sa vision des choses. C'est bien pour cette raison qu'un chef d'orchestre ne l'est pas pour un soir. Il dirige un ensemble pendant assez longtemps pour avoir une grande cohésion, des habitudes, une manière de communiquer qu'elle soit gestuelle ou explicative.

Les gestes, parlons-en !

### Une gestuelle codifiée 

Non, il ne brasse pas de l'air. Tout ce qu'il fait est pensé, réfléchit, mesuré et est sensé.

Sauf que, ce sens n'est pas défini "officiellement" dans le code de la musique. Tout ce code gestuel est défini durant les phases de nombreuses répétitions avec l'ensemble. C'est à ce moment-là qu'est construit ce langage implicite entre les musiciens et le chef d'orchestre. C'est aussi à ce moment-là qu'il construit la justesse et qu'il construit le son qu'il souhaite de chacune des parties instrumentales, ne pensez pas que l'intégralité de l'orchestre répète à chaque fois. Le chef d'orchestre peut décider d'être uniquement avec certains pupitres pour travailler une partie bien précise sur un axe bien précis, comme l'intensité de vibrato sur telle ou telle phrase de l'œuvre.

Ce code, même s'il tend à être un peu généralisé entre les chefs d'orchestre (surtout sur des éléments factuels), se divise en deux partie:

  - Des signaux basé sur des choses factuelles
  - Des signaux uniquement expressifs
  
Dans les signaux factuels, on retrouve plusieurs choses communes, mais pas forcément toutes. La vitesse ainsi que les variations de vitesse (le tempo), la mesure, les nuances (de hauteur de son ou de volume sonore), certains enchaînements d'accords (appelés des _progressions harmoniques_), le démarrage de telle ou telle partie soit de pupitre soit de soliste. En bref, toutes les indications qu'un musicien peut retrouver sur sa partition (à peu de choses près).

Les signaux expressifs eux, sont là pour donner le cœur, l'expression, l'émotion. Que ce soit la crispation des mains, des paumes vers le haut, vers le bas, un poing qui se ferme pour indiquer l'arrêt ou une immense souplesse des phalanges, mais aussi un regard particulier, un sourcil qui remonte... Tous ces signaux sont compris par les musiciens à force d'habitude et de coopération. C'est en ça que les phases de répétitions sont très formatrices et fédératrices d'un langage implicite entre les musiciens et le chef d'orchestre.

### La baguette

Rassurez-vous, il s'agit là d'un héritage historique et non pas d'une obligation. Et d'ailleurs, historiquement ce n'est pas une baguette, mais un grand bâton (nommé "bâton de direction") qui servait à battre la mesure, puis au fil des années, elle s'est transformée en baguette, qui elle aussi servait à battre la mesure sur le pupitre.

Certains en sont mort de ce bâton de direction d'ailleurs. Vous n'êtes pas sans connaitre Jean-Baptiste Lully, compositeur (et violoniste) de la période baroque (17e siècle et première moitié du 18e siècle). Il est mort de la gangrène, après s'être légèrement énervé et s'être donné malencontreusement un coup de bâton de direction sur un orteil. L'infection s'est propagée et il est mort quelques temps après. Moins de risque avec une baguette quand même, hormis se la mettre dans l'oeil.

Avec ou sans baguette, cela ne change pas fondamentalement la conduite d'orchestre. Il y a aussi une histoire de confort et bien sûr, d'image.

Pour terminer et surtout pour illustrer la chose, [regardez donc ceci](https://www.youtube.com/watch?v=WVbQ-oro7FQ) (YT), le chef d'orchestre est Gustavo Dudamel. Je n'ai pas pris cette vidéo parce que c'est le meilleur ou que l'œuvre me plaît, mais surtout parce que ce n'est pas immensément compliqué d'apercevoir au moins quand il donne le départ de certains pupitres, ainsi que quelques variations d'intensité et de rythme. Bien évidemment, ces gestes arrivent un petit peu avant la musique (autrement les musiciens n'auraient pas le temps d'interpréter les gestes).

Notez que la manière de filmer est similaire à un chef d'orchestre, il faut basculer sur la caméra des violons à telle mesure, histoire d'éviter de filmer des musiciens qui ne font rien. Il y a donc une dimension de métier de l'image certes, mais dans ce milieu une culture musicale est vitale.

## C'est une bonne situation ça, chef d'orchestre ?

Parlons d'argent. Il est vrai qu'on parle trop peu des salaires des musiciens et autres acteurs de ce domaine (parce que beaucoup crèvent la dalle et peu gagnent des millions mais pas forcément les plus talentueux) donc je me permets quelques mots, enfin, surtout quelques chiffres.

Le salaire d'un chef d'orchestre dépend surtout de quelques facteurs:

- Est-ce qu'il travaille pour un orchestre à dimension régionale, nationale, internationale ?
- Sa réputation sur le territoire ou dans le monde ?
- Est-il reconnu pour ce qu'il fait ?

Globalement on trouve trois niveaux avec des échelles de salaire:

Un chef d'orchestre en début de carrière se verra offrir un salaire entre 3.000€ et 5.000€ mensuel, ajouté à ceci entre 8.000 et 20.000 de cachet brut par concert. Un chef d'orchestre reconnu à l'international, ce qui n'est pas le cas de tout le monde, disposera en moyenne entre 4.000€ et 6.000€ mensuel, les cachets de concerts variant entre 20.000€ et 30.000€. Pour une notoriété incontestable, l'équivalent de Madonna, mais pour un chef d'orchestre, ça sera entre 6.000 et 8.000€ mensuel et les cachets de concerts peuvent dépasser les 50.000€.

Tout ceci sans parler des à côtés hein. Selon les statistiques, les chefs d'orchestres les mieux payés au monde dépassent le million d'euros par an.

## Conclusion

Outre le fait qu'un chef d'orchestre gagne plutôt bien sa vie, il est absolument vital à l'interprétation d'une œuvre. Véritable gardien des sentiments et doué d'une oreille impeccable, il assure la cohérence des instrumentistes sur le chemin de l'interprétation de part un langage codé fait de signes clairs qui peuvent s'apparenter à des ordres et des signes venant du cœur, des sentiments. 

Car avouons-le, si la musique est jouée mécaniquement, c'est qu'elle n'a plus d'âme. On peut dire en quelque sorte que le chef d'orchestre, de concert avec les musiciens, sont les gardiens et l'expression musicale de l'âme d'une œuvre.



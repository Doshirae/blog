---
authors: ["zilkos"]
title: Communauté
date: 2012-02-08
menu: "main"
toc: true
weight: 6000
---


Unixmail a toujours été ouvert, aussi bien dans les technologies employées pour faire le site que dans son contenu et notamment a licence appliquée. Ici, on part du principe que la culture, la création et l'expérience doivent être partagées librement entre tous.

Aussi, pour vous permettre de prendre part à l'aventure, plusieurs moyens s'offrent à vous pour venir parler, écrire et partager vos connaissances, vos expériences mais aussi le fruit de vos réflexions.

## Les outils communautaires

### Les mailing-list

Moyen simple de communiquer entre tous, Unixmail met à disposition des mailing-lists.

Au nombre de deux actuellement, elles sont les suivantes:

  * discussion: Pour tous les sujets possibles
  * support: Pour un coup de patte technique sur plein de choses

Pour l'inscription et savoir comment ça fonctionne ainsi que les règles qui régissent le service, je vous renvoie vers l'article de présentation, [c'est par ici](/informatique/mailing-list) !

### Le dépôt d'article (Git pour les intimes)

L'intégralité des pages de ce site sont dans un système de gestion de version de contenu. Ça permet plein de choses, notamment:

  - Un petit système de sauvegarde en cas de surprise
  - Un suivi de chaque modification de chaque article
  - Déclarer un problème sur un article (contenu, typo, lien cassé, etc)
  - Proposer une correction sur un article
  - Télécharger librement l'entièreté des articles du site

Vous avez libre choix de venir contribuer au site en proposant vos corrections, vos idées ou simplement venir signaler une erreur dans un article.

Notez que les contributeurs peuvent également rebondir sur vos corrections ou propositions pour améliorer les contenus !

{{% center %}}
**Le dépôt d'article est hébergé sur Gitlab, [par ici](https://gitlab.com/Zilkos/blog).**
{{% /center %}}
## Devenir auteur

Vous pouvez également venir écrire sur ce site si l'envie vous prend. Vous aurez:

  * Le choix de publier en votre nom ou pas
  * Le choix d'avoir une page avec votre nom d'auteur présentant tous vos écrits
  * Le choix sur la licence de votre contenu (Creative Commons uniquement)
  * Le choix de versionner vos articles ou non sur le dépôt
  * Le choix d'accepter des modifications ou propositions sur votre contenu

Grossièrement le site s'oriente sur quelques thèmes directeurs: l'informatique (tout domaine confondu), la musique, ou de simples histoires ou expériences personnelles, sociales. S'il faut créer un thème juste pour vous, aucun problème !

En bref, votre contenu, votre licence, vos choix.

Ça vous tente ? Si tel est le cas, n'hésitez pas à me contacter pour en discuter !

N'hésitez également pas à soumettre des idées d'articles qui pourraient être intéressants !

## Et les coûts dans tout ça ?

Comme vous l'avez remarqué, Unixmail ne gagne pas d'argent avec la pub ou le profilage, ce n'est pas notre politique en ces lieux. Unixmail est mis gratuitement au service de ses lecteurs et contributeurs depuis tant d'années et ça ne changera **jamais**. 

L'hébergement est financé par mes deniers personnels et le temps libre est mis à profit pour les articles et les mises à jour du contenu ainsi qu'à la gestion du site et des outils dédiés (mailing-list, dépôts d'articles) et d'autres choses que vous ne voyez pas (sauvegardes, pré-production, tests, évolutions, etc). Certes le temps libre est variable, donc les rythmes de publication le sont également.


### Dons

J'ai mis un système de don en place, via une plate-forme libre et respectueuse, [Liberapay](https://fr.liberapay.com/). Ils ont le bon goût de ne pas passer par Paypal ou autre système puant. Le but n'étant pas de s'enrichir bien évidemment (j'ai un travail) mais d'amortir légèrement le frais lié à ce blog, et, pourquoi pas, construire d'autres choses à l'avenir (devenir un [CHATONS](https://chatons.org/) par exemple).

{{% center %}}
Si le coeur vous en dit, n'hésitez pas à donner un coup de patte à hauteur de ce que vous voulez ! [C'est par ici](https://liberapay.com/Zilkos/donate).
{{% /center %}}

La gestion des dons est faite de manière complètement transparente et fera l'objet d'articles récurrents si dons il y a.
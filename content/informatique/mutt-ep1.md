---
authors: ["zilkos"]
title: 'Mutt: Historique et installation'
date: 2015-03-09
tags: ["Linux", "Mutt", "CLI", "Mail"]
toc: true
---

Une nouvelle série consacrée à un outil en console, le maître de tous les clients mails, j'ai nommé **Mutt** ! 
Dans cette première partie, on va faire un bref historique et voir comment l'installer sous différentes plates-formes, puis on posera des bases nécessaires pour la suite. 

En avant !


## "All mail clients sucks. This one just sucks less"

### Mutt, le lecteur de fichier

Mutt est ce que l'on nomme un courrielleur, un client e-mail, un logiciel de messagerie ou encore un MUA , en bref, ça désigne un logiciel qui sert à lire et à envoyer des courriers électroniques, aussi nommés e-mails ou courriels.

Mutt a vu le jour en 1995 de la main de _Mickael ELKINS_. Il est écrit en langage C et a été publié sous la licence _GNU GPL_ ([texte de la version 2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html), [texte de la version 3](http://www.gnu.org/licenses/gpl.html)), son développement est toujours actif (mais l'auteur originel n'est pas seul !). Le changelog est disponible à [cette adresse](http://www.mutt.org/doc/devel/ChangeLog). Le nom _Mutt_ vient de l'anglais signifiant _bâtard_ désignant un chien, d'où le logo. On peut penser que cela fait référence au chien qui va chercher le journal nonchalamment envoyé par un paperboy. Pour sûr, avec Mutt, vos courriers ne seront ni pleins de bave, ni déchirés !

Originellement, Mutt n'est pas un logiciel de messagerie à proprement parler car dans ses premières versions, il n'était pas capable d'envoyer ni de recevoir un mail. Il servait uniquement à **l'organisation** et à la **lecture** de mail (_plus précisément à la navigation et à la lecture de boite de courriel au format Maildir et Mbox_). Il fallait le coupler à différents outils, tels que `fetchmail` et `sendmail`. La première interface graphique de Mutt était ouvertement inspirée de celle du client [Elm](http://www.instinct.org/elm/). Mutt a hérité aussi de ce qui se faisait ailleurs tout au long de son développement, ainsi on y retrouve certaines fonctionnalités présentes dans [Pine](http://www.washington.edu/pine/), maintenant replacé par [Alpine](http://www.washington.edu/alpine/) notamment, un autre client mail en console relativement connu.

> Pour avoir testé les trois, Mutt est au final le plus abouti et celui possédant le plus de fonctionnalités et peut-être couplé à d'autres logiciels.

La documentation est principalement écrite et maintenue par Sven GUCKES. En ce qui concerne la documentation française, qui est une traduction de la documentation originale, c'est [Cédric DUVAL](http://cedricduval.free.fr/mutt/fr/) qui s'en occupe, avec les contributions de diverses personnes, notamment de [Gérard DELAFOND](http://www.delafond.org/traducmanfr/), bien connu pour plusieurs de ses traductions de pages man.

Dans les versions récentes, le paquet `mutt` supporte tout ce dont vous avez besoin pour récupérer et envoyer vos mails, sachez qu'il existe aussi le paquet `mutt-patched` qui contient quelques outils pour faire de Mutt votre fidèle allié pour encore très longtemps...


## Installation

### `mutt` ou `mutt-patched` ?

> **Mise à jour du 06/03/2018: Depuis la version 1.7 de Mutt, `mutt-patched` n'existe plus sur les distributions basées Debian mais les fonctionnalités sont présentes de base dans le paquet `mutt`, il convient donc de l'installer en lieu et place de `mutt-patched`.**

En voilà une question qu'elle est bonne !

Si vous avez une distribution GNU/Linux récente, la recherche du paquet mutt vous renverra plusieurs résultats, et notamment les deux suivants:

```
mutt - Outil de lecture de courriel en mode texte gérant MIME, GPG, PGP et les fils de discussion
mutt-patched - Mutt Mail User Agent with extra patches
```

Nous opterons pour **`mutt-patched`**. Pourquoi ? Pour faire court, `mutt` et `mutt-patched` c'est la même chose. `mutt-patched`, c'est juste `mutt` avec des options de compilation en plus, comme le support de la sidebar et d'autres choses.

#### Sur Debian et autres distributions communes

Il y a fort à parier qu'il soit présent dans vos dépôts. Que vous soyez sous Debian et dérivés (Ubuntu, Linux Mint, eOS), sous SuSe, Fedora, CentOS, vous devez le trouver simplement via la commande de recherche de paquet adaptée à votre distribution. Installez le paquet `mutt-patched`, il n'y a aucun piège.

#### Sur FreeBSD

Nos amis BSDistes (dont je fais partie) quant à eux disposent de plusieurs solutions...

Pour les fous souhaitant installer Mutt via les binaires pré-compilés et disposant d'un FreeBSD inférieur à la version 10, ça sera cette commande avec des droits super-utilisateur:

```bash
pkg_add -r mutt
```

Inutile de chercher `mutt-patched`, vu que Mutt pour FreeBSD inclus déjà les options de compilation en plus. Pour vous en assurer, vous pouvez aller consulter la page [FreshPort](http://www.freshports.org/mail/mutt/) de `mutt`, et constater que dans la liste, on rencontre :


```
SIDEBAR_PATCH=on: Sidebar support
```

ainsi que plein d'autres configurations comme le SMTP, les citations étendues, et même la gestion de la corbeille.

Pour ceux disposant d'une FreeBSD 10 ou supérieur et pour installer `mutt` via les binaires pré-compilés, ça sera cette commande avec des droits super-utilisateur:

```bash
pkg install mutt
```

Dans les deux cas, via les ports, ça sera cette commande, toujours avec les droits super-utilisateur:

```bash
cd /usr/ports/mail/mutt && make install clean
```

Quand il vous demandera les options de compilation, indiquez les mêmes quand dans la fiche [FreshPorts](http://www.freshports.org/mail/mutt/).

#### Sur Gentoo

Un simple `emerge` suffit ! Pour info, le retour de la commande `emerge -pv mutt` :

```
emerge -pv mutt
[ebuild N ] mail-client/mutt-1.5.21-r1 USE="gdbm gpg imap mbox nls nntp \
sidebar smime smtp ssl -berkdb -crypt -debug -doc -gnutls \
-idn -pop -qdbm -sasl -tokyocabinet"
```

### Configuration générale et .mailcap

La configuration sera le point qui nous prendra le plus de temps. Il y a énormément de manières de faire. Je vous en montrerai une basique, mais qui va jusqu'au bout. Par _jusqu'au bout_, j'entends du multi-compte, l'ouverture de pièce-jointe automatique, l'affichage des mails HTML, gestion d'un carnet d'adresse consultable depuis Mutt et bien évidemment, une impression des mails Mutt en PDF correctement formaté et tout joli.

En attendant, Mutt vient avec énormément de documentation pour sa configuration, je vous invite dès maintenant à aller consulter les nombreux exemples disponibles à l'emplacement `/usr/share/doc/mutt/examples` si vous êtes sous Debian et consorts.

Dans le prochain épisode, nous commencerons à construire notre premier `.muttrc` !

Nous verrons aussi comment se construit un fichier `.mailcap` digne de ce nom dans l'épisode 3 !

> Un fichier `.mailcap` est présent par défaut dans `/etc/mailcap` mais celui stocké dans `~/.mailcap` sera prioritaire. Ce fichier sert d'indication en couplant chaque type MIME avec une commande pour traiter les pièces jointes reçues. Typiquement, il dira: "_Pour ce type MIME, utilise ce logiciel_".

**Pour l'épisode 2, [c'est par ici !](/informatique/mutt-ep2)**
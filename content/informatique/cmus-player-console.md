---
authors: ["zilkos"]
title: 'Cmus: Un player en console'
date: 2013-08-28
tags: ["Linux", "CLI"]
---

Aujourd’hui, encore une découverte de logiciel en console qui fonctionne plus que très bien, j’ai nommé Cmus !

Cmus, sans doute pour console music est un player en console basé sur ncurses, plutôt satisfaisant. Certes, il ne remplacera pas mplayer, MOC, ou mp3blaster, mais il a son charme. Personnellement, j’ai pas 40 playlists selon mes humeurs, j’ai un tas de CD exportés sur mon OS, et je suis pas du genre à changer de morceau tout le temps. Je sais ce que j’ai, j’aime ce que j’ai, donc j’écoute tout. J’ai juste à choisir un CD.

Bref, installez donc Cmus (dans les dépôts) et lancez le via la commande `cmus` dans votre émulateur de terminal préféré.

### Ajoutons votre bibliothèque

Personnellement, elle est stockée dans `/home/moi/Musique`. Pour ouvrir le gestionnaire de fichier pour vous balader dans votre arborescence, appuyez sur 5. La ligne du haut fait l’équivalent d’un `cd ..`, donc placez vous sur le répertoire principale contenant toute votre musique.

Mon arborescence du répertoire Musique est comme ceci:

* Musique
    - Style 1
        + Artiste 1
        	- Album 1
        	- Album 2
        + Artiste 2
        	- Album 1
        	- Album 2
    - Style 2
        + Artiste 3

Et ainsi de suite. Une fois votre dossier contenant vos albums mis en surbrillance (donc Musique dans le cas présent) appuyez sur `a`. Voilà, Cmus a mis votre bibliothèque dans sa bibliothèque (sous forme de lien symbolique, rien n’est copié).

"_Et comment je le sais ?_"  
Bah, on va aller voir, ça sera plus simple.

### Votre bibliothèque

Pour accéder à votre bibliothèque, pressez 2. Mettez un morceau en surbrillance via les flèches et appuyez sur Entrée pour lancer la lecture.

Des petits raccourcis clavier pour vous aider:

* c pour lecture/pause
* flèche gauche/flèche droite pour avancer/reculer dans le morceau à intervalles de 10 sec
* `<` et `>` pour avancer/reculer avec l’intervalle à une minute
* `Shift + c` pour lire en continue (en bas à droite de votre écran il y a un C pour vous l’indiquer)
* `s` pour activer/désactiver le shuffle (en bas à droite de votre écran il y a un S pour vous l’indiquer)
* `r` pour activer/désactiver le repeat (en bas à droite de votre écran il y a un R pour vous l’indiquer)

### Faire une playlist

Tout simple ! Placez-vous dans votre bibliothèque en pressant la touche 2, puis pour chaque morceau que vous voulez ajouter à la playlist, surlignez le et appuyez sur y.

Pour aller voir et lire votre playlist, c’est la touche 3 ! Tout simple vous dis-je !

### Trouver un morceau

Pour les personnes disposant d’énormément de morceaux, ne pensez pas que chercher un morceau dans cette immense bibliothèque musicale soit compliqué. Passez sur  la bibliothèque (2) puis appuyez sur /. Dans le prompt en bas de la fenêtre, entrez votre texte par exemple Varèse (avouez le, vous êtes fan de musique expérimentale !). Pour passer à l’occurrence suivante de Varèse (ou l’élément en lien avec Varèse) appuyez sur la touche n (next : suivant).

### Et pour finir

Si vous êtes perdu ou si vous cherchez une commande / un raccourci, tapez 7, ne retenez pas tout, il y en a énormément ! Pour quitter, tapez :q (comme sous vim).


